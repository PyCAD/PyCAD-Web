import * as mocha from "mocha";
import * as chai from "chai";

import { foo } from "./"

const expect = chai.expect;

describe("Test test", () => {
    it("should equal \"bar\"", () => {
        expect(foo).to.equal("bar");
    });
});
