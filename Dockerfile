FROM node:10-alpine

RUN apk update
RUN apk add git

RUN git clone https://gitlab.com/PyCAD/PyCAD-Web.git /var/app

WORKDIR /var/app

RUN npm install

RUN npm run build

ENTRYPOINT ["cp", "-R", "/var/app/build/.", "/var/www/"]